﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace UDPNumberSender
{
    class UdpNumberSenderBroad
    {
        /*
            main() 
            Create a UdpClient udpSender, not on a specific port how ??
            Enable broadcast on the the udpSender
            Set up an IPEndpoint for broadcast
            Inside the while-loop 
            Use the IPEndpoint to send data
         */

        static void Main(string[] args)
        {
            Console.Title = "Client Sender board";

            UdpClient udpClient = new UdpClient();
            udpClient.EnableBroadcast = true;

            IPAddress ip = IPAddress.Broadcast;

            int number = 0;

            IPEndPoint RemoteIpEndPoint = new IPEndPoint(ip, 9999);


            while (true)
            {
                Console.WriteLine(number);
                Byte[] sendBytes = Encoding.ASCII.GetBytes("The number is:" + number);

                udpClient.Send(sendBytes, sendBytes.Length, RemoteIpEndPoint); //, (RemoteEndPoint NOT in 1-1 communication);

                number++;

                Thread.Sleep(100);
            }
        }
    }
}
